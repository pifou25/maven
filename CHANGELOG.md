# [3.1.0](https://gitlab.com/to-be-continuous/maven/compare/3.0.0...3.1.0) (2022-08-10)


### Bug Fixes

* **cache:** fix cache path ([831704e](https://gitlab.com/to-be-continuous/maven/commit/831704e75358e4ce923ae1130a04deb0d99c33bf))


### Features

* manage Sonar task cache in GitLab ([ba82d4c](https://gitlab.com/to-be-continuous/maven/commit/ba82d4cf4e9ef733311e2adc6bb9d24b6d7634eb))
* migrate $SONAR_AUTH_TOKEN to $SONAR_TOKEN (standard) ([841cedf](https://gitlab.com/to-be-continuous/maven/commit/841cedf8b693565ff9f02c650828de2b5d0e71f5))
* migrate $SONAR_URL to $SONAR_HOST_URL (standard) ([0edf601](https://gitlab.com/to-be-continuous/maven/commit/0edf6016dbcbe9e5b63d108736a0a75e02b8fbec))
* remove explicit MR analysis ([3db0b12](https://gitlab.com/to-be-continuous/maven/commit/3db0b125927ea8272b95b7e7533799f428aef520))
* remove support of Sonar GitLab plugin (discontinued) ([167cddd](https://gitlab.com/to-be-continuous/maven/commit/167cddd3b232b49c1f6e932c8bd2dba1a3f41bd9))
* standardize wait for quality gate impl ([d47e40d](https://gitlab.com/to-be-continuous/maven/commit/d47e40df018d30270912c57bfa69eb793b30b49b))

# [3.0.0](https://gitlab.com/to-be-continuous/maven/compare/2.3.0...3.0.0) (2022-08-05)


### Features

* adaptive pipeline ([08a1b5e](https://gitlab.com/to-be-continuous/maven/commit/08a1b5e56fab796f31eeb7f3a0ee28af98a765a3))


### BREAKING CHANGES

* change default workflow from Branch pipeline to MR pipeline

# [2.3.0](https://gitlab.com/to-be-continuous/maven/compare/2.2.0...2.3.0) (2022-05-01)


### Features

* configurable tracking image ([124762b](https://gitlab.com/to-be-continuous/maven/commit/124762bc16b31d7309b38faab58286745340c72b))

# [2.2.0](https://gitlab.com/to-be-continuous/maven/compare/2.1.6...2.2.0) (2022-04-28)


### Features

* Add a MAVEN_PROJECT_DIR ([7bdc6fe](https://gitlab.com/to-be-continuous/maven/commit/7bdc6feed564a6bf912b3a749f04bf2dd0c58a04))

## [2.1.6](https://gitlab.com/to-be-continuous/maven/compare/2.1.5...2.1.6) (2022-04-26)


### Bug Fixes

* migrate deprecated CI_BUILD_REF_NAME variable ([ed46369](https://gitlab.com/to-be-continuous/maven/commit/ed46369a84f43a320356ce44b0c7a3ebd595712e))

## [2.1.5](https://gitlab.com/to-be-continuous/maven/compare/2.1.4...2.1.5) (2022-04-02)


### Bug Fixes

* **dependency-check:** Use aggregate goal to support multi-modules projects ([bbddc72](https://gitlab.com/to-be-continuous/maven/commit/bbddc7248872315711cce4787b06ac2b5c00debd))

## [2.1.4](https://gitlab.com/to-be-continuous/maven/compare/2.1.3...2.1.4) (2022-02-25)


### Bug Fixes

* **artifacts:** always publish test artifacts ([3bcb0bc](https://gitlab.com/to-be-continuous/maven/commit/3bcb0bc547911bc08bd56f462d74159c0d4831da))

## [2.1.3](https://gitlab.com/to-be-continuous/maven/compare/2.1.2...2.1.3) (2022-02-04)


### Bug Fixes

* Use Dmaven.test.skip to avoid compiling and running tests instead of DskipTests ([d7c2da1](https://gitlab.com/to-be-continuous/maven/commit/d7c2da14743a385dd13783a953b8a140ab0bca27))

## [2.1.2](https://gitlab.com/to-be-continuous/maven/compare/2.1.1...2.1.2) (2021-10-07)


### Bug Fixes

* use master or main for production env ([d5cac44](https://gitlab.com/to-be-continuous/maven/commit/d5cac44ae499b9ab3063cdb31935a2cd9d1b448a))

## [2.1.1](https://gitlab.com/to-be-continuous/maven/compare/2.1.0...2.1.1) (2021-09-23)


### Bug Fixes

* mvn-release missing reusing .mvn-base before_script ([2d87f8d](https://gitlab.com/to-be-continuous/maven/commit/2d87f8d0563eee8b6fcfa147938337180e32fa70))

# [2.1.0](https://gitlab.com/to-be-continuous/maven/compare/2.0.1...2.1.0) (2021-09-13)


### Features

* auto-detect Maven settings file ([70f04e3](https://gitlab.com/to-be-continuous/maven/commit/70f04e3c4e9fb50b471fd1d832ffcfec1d76f411))

## [2.0.1](https://gitlab.com/to-be-continuous/maven/compare/2.0.0...2.0.1) (2021-09-07)

### Bug Fixes

* maven-enforcer-plugin version upgrade ([30dcc01](https://gitlab.com/to-be-continuous/maven/commit/30dcc012c26fcdaf38ac3596906717f095a6c6bd))

## [2.0.0](https://gitlab.com/to-be-continuous/maven/compare/1.4.2...2.0.0) (2021-09-03)

### Features

* Change boolean variable behaviour ([16ead86](https://gitlab.com/to-be-continuous/maven/commit/16ead8655048161dd52e6a699dd6a0f023e7d0d7))

### BREAKING CHANGES

* boolean variable now triggered on explicit 'true' value

Signed-off-by: Cédric OLIVIER <cedric3.olivier@orange.com>

## [1.4.2](https://gitlab.com/to-be-continuous/maven/compare/1.4.1...1.4.2) (2021-06-15)

### Bug Fixes

* **sonar:** prevent shallow git clone (required by Sonar Scanner) ([4dbd90e](https://gitlab.com/to-be-continuous/maven/commit/4dbd90e47805b09317702ba643929f88322b94df))

## [1.4.1](https://gitlab.com/to-be-continuous/maven/compare/1.4.0...1.4.1) (2021-06-15)

### Bug Fixes

* autodetect MR when a milestone is here ([c4fbdf3](https://gitlab.com/to-be-continuous/maven/commit/c4fbdf37d3e07f7980ee152b37f0e8978a2d129e))

## [1.4.0](https://gitlab.com/to-be-continuous/maven/compare/1.3.0...1.4.0) (2021-06-10)

### Features

* move group ([df3a46f](https://gitlab.com/to-be-continuous/maven/commit/df3a46f19e33da869ffcea9e7b879029ad915a21))

## [1.3.0](https://gitlab.com/Orange-OpenSource/tbc/maven/compare/1.2.0...1.3.0) (2021-06-07)

### Bug Fixes

* use curl instead of wget in get_latest_template_version script ([96b191f](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/96b191f8211e7957858ad58d2997f0a71391c534))

### Features

* **sonar:** autodetect Merge Request from current branch ([10c3058](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/10c3058fd379e1b989ac81b3caba84fbc347552c))

## [1.2.0](https://gitlab.com/Orange-OpenSource/tbc/maven/compare/1.1.2...1.2.0) (2021-05-18)

### Features

* add scoped variables support ([9089860](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/9089860cba63991fec586ddfe44304fe3e4df4c9))

## [1.1.2](https://gitlab.com/Orange-OpenSource/tbc/maven/compare/1.1.1...1.1.2) (2021-05-12)

### Bug Fixes

* make semrel integration disableable ([dd29f28](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/dd29f287da033a6945b4f75aa768c380120438ae))

## [1.1.1](https://gitlab.com/Orange-OpenSource/tbc/maven/compare/1.1.0...1.1.1) (2021-05-12)

### Bug Fixes

* **forbid-snapshot-dependencies:** use CLI options ([24cfb7b](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/24cfb7bdc5ef66717bb75de99784049744bd092b))

## [1.1.0](https://gitlab.com/Orange-OpenSource/tbc/maven/compare/1.0.0...1.1.0) (2021-05-07)

### Features

* add forbid snapshot dependencies job ([295f385](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/295f38530193f62876f6167fa5fd118c4fa5119c))
* add semantic-release integration ([d99c6bb](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/d99c6bbf811d37e7e7152063f5f1fc5de230c37a))

## 1.0.0 (2021-05-06)

### Features

* initial release ([67ee980](https://gitlab.com/Orange-OpenSource/tbc/maven/commit/67ee980ac5acf69b9bf9cf3c71d7a2d9c1385bd1))
